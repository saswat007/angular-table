import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-inputform',
  templateUrl: './inputform.component.html',
  styleUrls: ['./inputform.component.css'],
})
export class InputformComponent implements OnInit {
  userForm: FormGroup;
  listdata: any;
  // namepattern = "^[a-zA-Z]+$";
  emailpattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$';
  phnpattern = '^((\\+91-?)|0)?[0-9]{10}$';

  constructor(private _fb: FormBuilder) {
    this.listdata = [];

    this.userForm = this._fb.group({
      fname: ['', [Validators.required, Validators.minLength(3)]],
      lname: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.pattern(this.emailpattern)]],
      mob: ['', [Validators.required, Validators.pattern(this.phnpattern)]],
    });
  }

  ngOnInit(): void {}

  // additem() {
  //   console.log(this.userForm.value.email);

  //   this.listdata.forEach((value, index) => {

  //     if (value.email == this.userForm.value.email) {
  //       console.log("inside if");
  //       this.listdata.splice(index, 1);
  //       this.listdata.push(this.userForm.value);
  //     }else
  //     {
  //       this.listdata.push(this.userForm.value);
  //     }
  //   });
  //   this.listdata.push(this.userForm.value);
  //   this.userForm.reset();
  // }

  additem() {
    console.log(this.userForm.value);
    this.listdata.push(this.userForm.value);
    this.userForm.reset();
  }

  reset() {
    this.userForm.reset();
  }

  remove(element) {
    console.log(element)
    console.log('removal success');
    this.listdata.forEach((value, index) => {
      if (value == element) {
        console.log("inside if");
        this.listdata.splice(index, 1);
        //this.listdata.push(element);
      }
    });
  }
  edit(element) {
    console.log('in edit');
    this.listdata.forEach((value, index) => {
      if (value == element) {
        this.userForm.setValue({
          fname: element.fname,
          lname: element.lname,
          email: element.email,
          mob: element.mob,
        });
        this.listdata.splice(index, 1);
      }
    });
  }
}
